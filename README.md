# Abschlussprojekt - Marlon Drolsbach, Patrick Hay

## Installation
Zur Installation sind Python3 und pip erforderlich
```
git clone git@git.thm.de:psdt11/sse.git
cd sse
pip install -r requirements.txt
python main.py
```
## Beschreibung
Bei der Anwendung handelt es sich um eine Webapp zum Anlegen von Notizen. Es ist möglich private sowie öffentliche Notizen zu erstellen. Außerdem kann nach ihnen gesucht werden.
## Verwendete Technologien
- Python3
- Flask 
- SQLite
- Bootstrap
- Visual Studio Code
- Jetbrains WebStorm
- Jetbrains PyCharm
### [Flask](https://github.com/pallets/flask/)
- BSD-3-Clause License
- Used by 1.1M
- 672 Contributors
- Regelmäßige Updates
- Umfangreiche Dokumentation und Tutorials
- Keine bekannten Sicherheitslücken
Daher eignet sich flask sehr gut für unsere Anwendungszwecke
## Entwicklungsprozess
Zum Entwickeln der Webapp wurde sich grob an den Feature-Branch-Workflow gehalten. Für die Implementierung eines neuen Features wurde ein neuer Branch eröffnet, welcher nach Abschluss in den Master-Branch einfloss.
## Umsetzung
### Registrierung/Anmeldung
Die Anmeldung erfolgt mithilfe einer Email-Adresse und eines Passworts. Alle Nutzerdaten werden in einer SQLlite-Datenbank gespeichert. Sensitive Daten wie das Passwort werden gehasht gespeichert. Bei der Registrierung wird anhand verschiedener Kriterien geprüft, ob das Passwort sicher genug ist. Da mit der Datenbank kommunziert wird, werden Prepared Statements eingesetzt, um vor SQL-Injections zu schützen.
### Autorisierung
Zur Autorisierung von Nutzern verwendet das Backend die Flask-Funktion "Session". Auf Nutzerseite wird ein Session-Token als Cookie abgespeichert, sodass man für alle weiteren http-Anfragen autorisiert ist.
### Notizen
Notizen können in Markdown verfasst werden. Zur Umwandlung von Markdown in lesbaren formatierten Text verwendet die Webapp das Python-Modul "markdown". Gibt ein Nutzer an, dass eine Notiz privat sein soll, wird dies inklusive User-ID in der Datenbank vermerkt. So wird bei jedem Aufruf der Notiz sichergestellt, dass nur der Ersteller Zugriff darauf hat. Jede Notiz hat eine einzigartige ID, welche in der URL angegeben werden kann um sie zu teilen und darauf zuzugreifen. (z.B. https://my-app.de/documents/e0ff5914-43e4-4d48-8a4c-5ef7845c7951) Umgesetzt wurde diese Funktion mithilfe der Routing-Funktion von flask.
### Suche
Es ist auch möglich, die eigenen Notizen zu durchsuchen nach Stichwörtern. Bei der Suche wird der Suchbegriff in der URL-Angegeben und auf der entsprechenden Seite angezeigt. Hier wurde ebenfalls das Routing-Feature von flask verwendet. In der Datenbank wird dann nach Notizen mit dem gesuchten Begriff im Titel gesucht. Außerdem muss die User-ID des Suchenden dazu passen. 

## Mögliche Schwachstellen
### XSS
Bei einer Webanwendung besteht die Möglichkeit, dass ein Angreifer versucht seinen eigenen, schädlichen Code in die 
Anwendung einszuschleusen. Im Fall von Webanwendungen, die in einem Browser als Client laufen passiert dies über das 
einpflegen von JavaScript-Code. Bei jeder Eingabe von einem Benutzer kann es sein, dass auch potentiell schädlicher Code
eingebracht werden könnte. <br>Aufgrund dessen ist es notwendig diesen Vektor abzuschirmen, da jeder Client bei einer 
Eingabe von Script-Code diesen ausführen wird. Um einen Schutz dagegen zu erreichen ist es möglich HTML Code und damit auch inline JavaScript-Code "unschädlich" zu machen indem die Zeichen codiert werden. So wird das HTML-Tag für ein Script escaped. Auf diese Weise wird der Code nicht mehr im Browser ausgeführt, kann aber dargestellt werden. 
### SQL-Injection
Da an vielen Stellen in der Webapp Nutzereingaben an die Datenbank weitergegeben werden, ist die Anwendung möglicherweise anfällig für SQL-Injections. Um diese zu verhindern verwenden wir Prepared Statements. Diese kompilieren und verarbeiten die Queries für die Datenbank zuerst und setzen als letztes die Nutzereingaben hinzu. So lässt sich kein Schadcode in die Queries integrieren.
### Bruteforce
Um einen möglichen Bruteforceangriff abzuwehren speichert die Anwendung die Anzahl von fehlgeschlagenen Loginversuchen. 
Dazu wird, ebenso wie bei einem erfolgreichen Login, über ein Cookie die Anzahl gespeichert und bei den Versuchen zusätzlich mit abgefragt. Bei zu vielen fehlgeschlagenen Versuchen blockiert die Anwendung weitere Loginversuche.
## Datenschutz
Für das sichere Speichern von Nutzerkennwörtern in der Datenbank werden diese vor dem Speichern gehasht. Dazu verwendet
die Anwendung den PBKDF2-Algorithmus mithilfe von SHA-256. Über SHA-256 wird inklusive Salt 310.000 mal iteriert. Das 
gehashte Passwort und Salt werden dann in der Datenbank gespeichert.  
Im Backend werden die Nutzer über ihre User-ID identifiziert. Um Nutzer zu Autorisieren wird über das Session-Feature 
von Flask eine Session erstellt. Jeder Session ist eine User-ID zugeteilt. Auf Nutzerseite wird ein Session-Token als 
Cookie gespeichert. Möchte jemand nun auf private Dokumente zugreifen, die nicht seiner User-ID und Damit seinem Session-Token zugeordnet sind, erhält er nur die Meldung, dass kein Dokument gefunden wurde.
