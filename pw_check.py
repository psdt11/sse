import re
def check_password(password):

    if len(password)<8:
        return 0
    if re.search('^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!\-%*?&]{8,}$',password) is None: #!$%&*?@
        return 0
    pw_list = open("pw_list.txt", "r").read().splitlines()
    pattern_list = open("patterns.txt", "r").read().splitlines()
    names_list = open("names_list.txt", "r").read().splitlines()

    if re.search('(.)\\1{3,}', password) is not None:
            return 0

    if password in pw_list:
        return 0
    for name in names_list:
        if re.search(f'{name}(19|20)\d\d', password) is not None:
            return 0
    for pattern in pattern_list:
        if re.search(f'{pattern}', password) is not None:
            return 0
    return 1
    