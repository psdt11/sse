from pydoc import getdoc
import sqlite3
import uuid
import os
import hashlib
import pw_check


def register(email, username, password):
    con = sqlite3.connect('database.sqlite')
    cur = con.cursor()
    if len(cur.execute('SELECT * FROM users WHERE email == ?', (email,)).fetchall()) > 0:
        print("Email vergeben")
        return -1

    if len(cur.execute('SELECT * FROM users WHERE username == ?', (username,)).fetchall()) > 0:
        print("Username vergeben")
        return -2

    if pw_check.check_password(password) == 0:
        print("Passwort schwach")
        return -3

    # passwort check
    # passwort hashen

    salt = os.urandom(32)
    key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 310000)

    #cur.execute(f'INSERT INTO users(email,username,password) VALUES("{email}", "{username}", "{key}", "{salt}")')
    cur.execute('INSERT INTO users(email,username,password,salt) VALUES(?, ?, ?, ?)', (email, username, key, salt))
    con.commit()
    con.close()


def login(email, password):
    con = sqlite3.connect('database.sqlite')
    cur = con.cursor()

    if len(cur.execute('SELECT * FROM users WHERE email == ? ', (email,)).fetchall()) != 1:
        print("Email existiert nicht")
        return -1

    for row in cur.execute('SELECT * FROM users WHERE email == ?', (email,)):
        user_id, _, _, db_password, salt = row
        # passwort hashen
        key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 310000)
        if key != db_password:
            print("Passwort falsch")
            return -1
        else:
            return user_id
    # check db
    # wrong/right
    # session token


def getDocument(docId, user_id):
    con = sqlite3.connect('database.sqlite')
    cur = con.cursor()
    # existiert?
    if len(cur.execute('SELECT * FROM documents WHERE document_id == ?', (docId,)).fetchall()) != 1:
        print("Dokument existiert nicht")
        return None
    for row in cur.execute('SELECT * FROM documents WHERE document_id == ?', (docId,)):
        document_id, title, body, doc_user_id, private = row
    if private == 1 and user_id != doc_user_id:
        print("Dokument ist nicht öffentlich")
        return None
    return (document_id, title, body, doc_user_id, private)

    # check public
    # if not check user "not found"
    # get doc from db


def addDocument(title, body, private, user_id):
    # random id
    con = sqlite3.connect('database.sqlite')
    cur = con.cursor()

    document_id = str(uuid.uuid4())
    cur.execute('INSERT INTO documents VALUES(?, ?, ?, ?, ?)', (document_id, title, body, user_id, private))
    con.commit()
    con.close()
    # add to database


def getMyDocument(user_id):
    con = sqlite3.connect('database.sqlite')
    cur = con.cursor()
    res = []
    for row in cur.execute('SELECT * FROM documents WHERE user_id == ?', (user_id,)):
        res.append(row)
    return res


def getMyDocument(user_id):
    con = sqlite3.connect('database.sqlite')
    cur = con.cursor()
    res = []
    for row in cur.execute('SELECT * FROM documents WHERE user_id == ?', (user_id,)):
        res.append(row)
    return res


def searchDocument(title, user_id):
    con = sqlite3.connect('database.sqlite')
    cur = con.cursor()
    res = []
    for row in cur.execute('SELECT * FROM documents WHERE title LIKE "%"||?||"%" AND user_id == ?',(title, user_id,)):
        res.append(row)
    return res


if __name__ == "__main__":
    login("mail@mail.com", "THM12345689")
    pass
