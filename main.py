import db_connection as database
import markdown
from flask import Flask, render_template, redirect, url_for, request, session
import re
import html

app = Flask(__name__)
app.secret_key = "ohwell"


@app.route("/")
# Erste Seite für den Start
def home():
    return render_template("index.html")


@app.route("/login")
# Route um sich einzuloggen. Für den Fall, dass es eine Warnung gibt, wird diese aus der URL extrahiert und gerendert.
# Eingabefelder für den Log-in
def login():
    return render_template("login.html")


@app.route("/login/pending", methods=["POST"])
# Wenn sich ein Benutzer einloggen will wird er hier hingeleitet. Er kommt nur über einen POST-Request hier hin
def pending():
    if not "amount" in session:
        session["amount"] = 0
    if request.method == "POST":
        # Daten werden aus dem Formular geholt
        mail = html.escape(request.form["mail"])
        pw = html.escape(request.form["password"])
        # Daten werden aus der Datenbank geholt. Fall -1 zurückgegeben, dann Fehler, falls nicht => User-ID
        if check_mail(mail):
            log = database.login(mail, pw)
        else:
            return render_template("login.html",
                                   warning_msg="Es muss eine korrekte Mailadresse eingegeben werden (max@mustermann.de)")
        if log == -1 and session["amount"] < 3:
            session["amount"] = session["amount"] + 1
            return render_template("login.html", alert_msg="E-mail oder Passwort falsch! Sie haben noch " + str(
                3 - session["amount"]) + " Versuche!")
        elif log == -1 and session["amount"] >= 3:
            return render_template("index.html", locked=True)
        else:
            session["usr_id"] = log
            session.pop("amount", None)
            return render_template("success.html",
                                   content="Sie sind eingeloggt. Sie können jetzt Notizen schreiben.")
    return render_template("login.html")


@app.route("/register", methods=["POST", "GET"])
# Route um einen neuen Benutzer zu registrieren
def register():
    if request.method == "POST":
        pwd = html.escape(request.form["password"])
        pwd_check = html.escape(request.form["password_check"])
        user = html.escape(request.form["name"])
        mail = html.escape(request.form["mail"])
        if pwd == pwd_check and check_mail(mail):
            info = database.register(mail, user, pwd)
            if info in [-1, -2]:
                return render_template("register.html",
                                       alert_msg="Sie konnten nicht registriert werden. Benutzername oder E-mail ist schon vergeben.")
            elif info == -3:
                alert_msg = ["Sie konnten nicht registriert werden. Das Passwort erfüllt nicht die Voraussetzungen.",
                             "Mindestens ein Groß/Kleinbuchstabe.",
                             "Mindestens eine Ziffer 0-9.",
                             "Mindestens ein Sonderzeichen (!$%&*?@).",
                             "Keine mehrfache Wiederholung (z.B aaaaa oder 3333).",
                             "Keine Namen mit Geburtsdaten (z.B Bobby1975!).",
                             "Keine Tastaturmuster (z.B. asdfghjkl)."]
                return render_template("register.html",
                                       alert_msg_lst=alert_msg)
            return render_template("success.html",
                                   content="Sie wurden registriert. Sie können sich jetzt einloggen.")
        else:
            return render_template("register.html",
                                   warning_msg="Die eingegeben Passwörter sind nicht identisch oder es wurde keine Mailadresse eingegeben (max@mustermann.de)")
    else:
        return render_template("register.html")


@app.route("/logout")
# Ein Benutzer kann sich hier ausloggen
def logout():
    session.pop("usr_id", None)
    return redirect(url_for("login"))


@app.route("/search")
def search():
    if "usr_id" in session:
        queue = request.args.to_dict()
        variable = html.escape(queue.get("q"))
        print(variable)
        document = database.searchDocument(variable, session["usr_id"])
        print(document)
        if document:
            return render_template("noteshowing.html", result=change_to_md(document),
                                   showing=True,
                                   searched=html.unescape(variable))
        else:
            return render_template("fail.html",
                                   content="Zu Ihrer Suche gibt es keine Ergebnisse!")
    else:
        return render_template("login.html", warning_msg="Sie sind nicht eingeloggt!")


@app.route("/note/writing", methods=["POST", "GET"])
def write_note():
    if "usr_id" in session:
        if request.method == "POST":
            subject = html.escape(request.form["subject"])
            content = html.escape(request.form["note"])
            private = 0
            try:
                request.form["priv"]
            except:
                pass
            else:
                private = 1
            database.addDocument(subject, content, private, session["usr_id"])
            return redirect(url_for("show_all_notes"))
        else:
            return render_template("notewriting.html")
    else:
        return render_template("login.html", warning_msg="Sie sind nicht eingeloggt!")


@app.route("/note/showing")
def show_all_notes():
    if "usr_id" in session:
        document = database.getMyDocument(session["usr_id"])
        return render_template("noteshowing.html",
                               result=change_to_md(document),
                               showing=True)
    else:
        return render_template("login.html", warning_msg="Sie sind nicht eingeloggt!")


@app.route("/documents/<ident>")
def show_note(ident):
    if "usr_id" in session:
        document = database.getDocument(ident, session["usr_id"])
        if document:
            return render_template("noteshowing.html",
                                   result=change_to_md(document),
                                   showing=False)
        else:
            return render_template("fail.html",
                                   content="Kein Dokument gefunden!")
    else:
        return render_template("login.html", warning_msg="Sie sind nicht eingeloggt!")


def change_to_md(given):
    # Methode um die Daten aus der Datenbank in Markdown-Input zu rendern und zurückzugeben
    returning = []
    if isinstance(given, list):
        for note in given:
            tmp = list(note)
            first = re.search("&lt;script&gt;", tmp[1])
            second = re.search("&lt;script&gt;", tmp[2])
            if not first and not second:
                print("In der doppelten IF")
                tmp[1] = html.unescape(markdown.markdown(tmp[1]))
                tmp[2] = html.unescape(markdown.markdown(tmp[2]))
            elif not first and second:
                print("In der IF für #1")
                tmp[1] = html.unescape(markdown.markdown(tmp[1]))
            elif first and not second:
                print("In der IF für #2")
                tmp[2] = html.unescape(markdown.markdown(tmp[2]))
            returning.append(tmp)
    else:
        first = re.search("&lt;script&gt;", given[1])
        second = re.search("&lt;script&gt;", given[2])
        if not first and not second:
            returning.append(html.unescape(markdown.markdown(given[1])))
            returning.append(html.unescape(markdown.markdown(given[2])))
        elif not first and second:
            returning.append(html.unescape(markdown.markdown(given[1])))
            returning.append(markdown.markdown(given[2]))
        elif first and not second:
            returning.append(markdown.markdown(given[1]))
            returning.append(html.unescape(markdown.markdown(given[2])))
        else:
            returning.append(markdown.markdown(given[1]))
            returning.append(markdown.markdown(given[2]))
    return returning


def check_mail(mail):
    pattern = ".*@[a-z]*\.[a-z]{2,}"
    check = re.search(pattern, mail)
    if check:
        return True
    else:
        return False


if __name__ == "__main__":
    app.run(debug=True)
